<?php

namespace App\Http;

use Rareloop\Lumberjack\Http\Lumberjack as LumberjackCore;
use App\Menu\Menu;

class Lumberjack extends LumberjackCore
{
    public function addToContext($context)
    {
        $context['is_home'] = is_home();
        $context['is_front_page'] = is_front_page();
        $context['is_logged_in'] = is_user_logged_in();
        $context['home_url'] = get_home_url();

        // In Timber, you can use TimberMenu() to make a standard Wordpress menu available to the
        // Twig template as an object you can loop through. And once the menu becomes available to
        // the context, you can get items from it in a way that is a little smoother and more
        // versatile than Wordpress's wp_nav_menu. (You need never again rely on a
        // crazy "Walker Function!")
        if(is_user_logged_in()){
            $context['menu'] = new Menu('main-nav');
        } else {
            $context['menu'] = new Menu('main-nav-logged-out');
        }
        $context['site_links'] = new Menu('footer-link');
        $context['resource_links'] = new Menu('resource-hub');

        $context['phone'] = get_field('phone', 'option');
        $context['email'] = get_field('email', 'option');
        $context['instagram'] = get_field('instagram', 'option');
        $context['facebook'] = get_field('facebook', 'option');

        $context['social_bar_heading'] = get_field('social_heading', 'option');
        $context['social_bar_text'] = get_field('social_text', 'option');
        $signup_form = get_field('social_form', 'option');
        $context['social_bar_form'] = do_shortcode('[gravityform id="'.$signup_form.'" title="false" description="true" ajax="true"]');
        $context['footer_end'] = get_field('footer_small_print', 'option');
        $context['top_bar'] = get_field('top_bar', 'option');

        if(getenv('WP_ENV') === 'production' ){
            // if ( function_exists( 'gdpr_cookie_is_accepted' ) ){
            //     if ( gdpr_cookie_is_accepted( 'thirdparty' ) ){

                    $context['gtm'] = "<!-- Google Tag Manager -->
                        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                        })(window,document,'script','dataLayer','GTM-WTP5BZC');</script>
                        <!-- End Google Tag Manager -->";

                    $context['gtm_scripts'] = '<!-- Google Tag Manager (noscript) -->
                        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KFK6G2V"
                        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
                        <!-- End Google Tag Manager (noscript) -->';

            //     }
            // }

            // $context['fathom'] = '<!-- Fathom - beautiful, simple website analytics -->
            //                         <script src="https://cdn.usefathom.com/script.js" site="FCCYIOVV" defer></script>
            //                         <!-- / Fathom -->';
        }
        
        return $context;
    }
}
