<?php

namespace App\Http\Controllers;

use Timber\Timber;
use Rareloop\Lumberjack\Post;
use App\PostTypes\ClientZone;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;

class ClientZoneController
{

    public function __construct()
    {
        add_filter('wp_title', function ($title) {
            return 'Clint Zone - Nimbus Collective';
        });
    }

    public function handle()
    {
        $context = Timber::get_context();

        $context['title'] = 'Client Zone';
        $context['banner']['heading'] = 'Client Zone';

        if($context['is_logged_in']){
            $user_id = get_current_user_id();

            $posts = ClientZone::builder()
            ->whereMeta('client', $user_id)
            ->get();

            if (!empty($posts[0])) {
                $page_id = $posts[0]->id;
                $page = new ClientZone($page_id);

                $context['post'] = $page;
                $context['content'] = $page->content;
                $context['downloads'] = get_field('download', $page_id);
            } else {
                $context['content'] = "<p>If you've purchased custom resources this is where they'll show up.</p><p>If you want custom resources please <a href='/say-hi/'>get in touch</a>.</p>";
            }
            
        } else {
            $context['content'] = "<p>If you've purchased custom resources this is where they'll show up.</p><p>If you want custom resources please <a href='/say-hi/'>get in touch</a>.</p>";
        }

        // $context['post'] = $page;
        
        // $context['content'] = $page->content;

        return new TimberResponse('basic', $context);
    }

    public function show(){
    	return test;
    }
}