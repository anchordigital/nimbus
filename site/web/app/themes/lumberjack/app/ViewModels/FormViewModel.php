<?php

namespace App\ViewModels;

use Timber\Post;
use Rareloop\Lumberjack\ViewModel;

class FormViewModel extends ViewModel
{
    protected $post;

    public function __construct(Post $post, $signup = false)
    {
        $this->post = $post;
        $this->signup = $signup;
    }

     public function id(): int
    {
        return $this->post->id();
    }

    public function heading(): string
    {
        return get_field('heading', $this->post->id());
    }

    public function description(): string
    {
        return get_field('form_explainer', $this->post->id());
    }

    public function send_to(): string
    {
        return get_field('send_to', $this->post->id());
    }

    public function subject_line(): string
    {
        return get_field('subject_line', $this->post->id());
    }

    public function fields(): array
    {
        return get_field('fields', $this->post->id());
    }

    public function button_text(): string
    {
        return get_field('submit_button_text', $this->post->id());
    }

    public function add_to_mailing_list()
    {
        // Get the price from the ACF field for this product
        return get_field('add_to_mailing_list', $this->post->id());
    }

    public function display_opt_in()
    {
        // Get the price from the ACF field for this product
        return get_field('display_opt_in', $this->post->id());
    }

    public function opt_in_text()
    {
        // Get the price from the ACF field for this product
        return get_field('opt_in_legal_text', $this->post->id());
    }

    public function success_message(): string
    {
        return get_field('success_message', $this->post->id());
    }

    public function hide_label(): bool
    {
        if($this->signup == true){
            return true;
        } else {
            return false;
        }
    }

    public function signup(): bool
    {
        return $this->signup;
    }

    public function hide_heading(): bool
    {
        if($this->signup == true){
            return true;
        } else {
            return false;
        }
    }

}