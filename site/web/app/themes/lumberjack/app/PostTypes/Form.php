<?php

namespace App\PostTypes;

use Rareloop\Lumberjack\Post;

class Form extends Post
{
    /**
     * Return the key used to register the post type with WordPress
     * First parameter of the `register_post_type` function:
     * https://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @return string
     */
    public static function getPostType()
    {
        return 'Form';
    }

    /**
     * Return the config to use to register the post type with WordPress
     * Second parameter of the `register_post_type` function:
     * https://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @return array|null
     */
    protected static function getPostTypeConfig()
    {
        return [
            'labels' => [
                'name' => __('Forms'),
                'singular_name' => __('Form'),
                'add_new_item' => __('Add New Form'),
            ],
            'public' => true,
            'menu_icon' => 'dashicons-text-page',
            'show_in_rest' => true,
            'supports' => array( 'title' ),
            'has_archive' => false,
            'publicly_queryable'  => false,
            'rewrite' => array( 'with_front' => false ),
        ];
    }

    public function heading()
    {
        // Get the price from the ACF field for this product
        return get_field('heading', $this->id);
    }

    public function form()
    {
        // Get the price from the ACF field for this product
        return get_field('columns', $this->id);
    }

    public function form_explainer()
    {
        // Get the price from the ACF field for this product
        return get_field('form_explainer', $this->id);
    }

    public function add_to_mailing_list()
    {
        // Get the price from the ACF field for this product
        return get_field('add_to_mailing_list', $this->id);
    }

    public function display_opt_in()
    {
        // Get the price from the ACF field for this product
        return get_field('display_opt_in', $this->id);
    }

    public function opt_in_text()
    {
        // Get the price from the ACF field for this product
        return get_field('opt_in_legal_text', $this->id);
    }

    public function button_text()
    {
        // Get the price from the ACF field for this product
        return get_field('submit_button_text', $this->id);
    }

}
