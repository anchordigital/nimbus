<?php

namespace App\PostTypes;

use Rareloop\Lumberjack\Post;

class ClientZone extends Post
{
    /**
     * Return the key used to register the post type with WordPress
     * First parameter of the `register_post_type` function:
     * https://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @return string
     */
    public static function getPostType()
    {
        return 'ClientZone';
    }

    /**
     * Return the config to use to register the post type with WordPress
     * Second parameter of the `register_post_type` function:
     * https://codex.wordpress.org/Function_Reference/register_post_type
     *
     * @return array|null
     */
    protected static function getPostTypeConfig()
    {
        return [
            'labels' => [
                'name' => __('Client Zone'),
                'singular_name' => __('Client Zone'),
                'add_new_item' => __('Add New Client Zone'),
            ],
            'public' => true,
            'menu_icon' => 'dashicons-thumbs-up',
            'show_in_rest' => true,
            'has_archive' => false,
            'publicly_queryable' => false,
            'supports' => array( 'permalink', 'title', 'editor' )
        ];
    }

}
