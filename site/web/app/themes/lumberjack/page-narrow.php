<?php

/**
 * Template name: Narrow
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Page;
use Timber\Timber;

class PageNarrowController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $page = new Page();

        $context['post'] = $page;
        $context['title'] = $page->title;
        $context['content'] = $page->content;

		require_once('template-builders/banner.php');

		require_once('template-builders/sections.php');

        return new TimberResponse('generic-narrow', $context);
    }
}
