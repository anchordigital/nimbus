<?php
$context['banner']['heading'] = get_field('banner_heading');
if(get_field('banner_button')){
	$context['banner']['button'] = get_field('banner_button');
}
if(get_field('responsive_images') == 'yes'){
	$context['banner']['image_small'] = get_field('banner_image_small');
	$context['banner']['image_medium'] = get_field('banner_image_medium');
	$context['banner']['image_large'] = get_field('banner_image_large');

} else {
	$context['banner']['image_small'] = get_field('banner_image');
	$context['banner']['image_medium'] = get_field('banner_image');
	$context['banner']['image_large'] = get_field('banner_image');
}
?>