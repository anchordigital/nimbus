<?php
// Get flexible field data
use Rareloop\Lumberjack\Page;


$context['sections'] = array();

 if (have_rows('sections')) :

    while (have_rows('sections')) : the_row();

        $array = array(
            "id" => get_sub_field("id"),
            "background" => get_sub_field("background"),
            "bottom_padding" => get_sub_field("bottom_padding"),
            "blocks" =>  array()
        );
        
        if (have_rows('content_block')) :

            while (have_rows('content_block')) : the_row();

                $blocks = array(
                    "size" => get_sub_field("size"),
                    "content" => array(),
                );

                if (have_rows('content')) :

                    while (the_flexible_field("content")) :

                        // If text block
                        if (get_row_layout() == "heading") :

                            $content = array(
                                "type" => "heading",
                                "heading" => get_sub_field("heading"),
                                "heading_size" => get_sub_field("heading_size")
                            );

                            // Push item into array ready for outputting
                            array_push($blocks['content'], $content);

                        elseif (get_row_layout() == "text") :

                            $content = array(
                                "type" => "text",
                                "text" => get_sub_field("text"),
                            );

                            // Push item into array ready for outputting
                            array_push($blocks['content'], $content);

                        elseif (get_row_layout() == "button") :

                            $content = array(
                                "type" => "button",
                                "alignment" => get_sub_field("alignment"),
                                "colour" => get_sub_field("colour"),
                                "button" => get_sub_field("button"),
                            );

                            // Push item into array ready for outputting
                            array_push($blocks['content'], $content);

                        elseif (get_row_layout() == "image") :
                            $image = get_sub_field('image');
                            $content = array(
                                "type" => "image",
                                "image" => wp_get_attachment_image($image['id'], 'full'),
                                "image_alignment" => get_sub_field("image_alignment"),
                            );

                            // Push item into array ready for outputting
                            array_push($blocks['content'], $content);
                        
                        elseif (get_row_layout() == "embed") :

                            $content = array(
                                "type" => "embed",
                                "embed" => get_sub_field("embed"),
                            );

                            // Push item into array ready for outputting
                            array_push($blocks['content'], $content);

                        elseif (get_row_layout() == "form") :

                            $form = new Form(get_sub_field("form"));

                            $content = array(
                                "type" => "form",
                                "form" => new FormViewModel($form)
                            );

                            // Push item into array ready for outputting
                            array_push($blocks['content'], $content);

                        endif;

                    endwhile;

                endif;

                array_push($array['blocks'], $blocks);

            endwhile;

            array_push($context['sections'], $array);

        endif;

    endwhile;

endif;

