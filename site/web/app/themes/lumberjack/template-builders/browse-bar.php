<?php
$context['product_themes'] = get_terms( 'ad_product_theme', array(
    'hide_empty' => true,
) );

$context['product_cats'] = get_terms( 'product_cat', array(
    'hide_empty' => true,
) );

$context['product_context'] = get_terms( 'ad_product_context', array(
    'hide_empty' => true,
) );
?>