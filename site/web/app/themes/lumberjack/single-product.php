<?php

/**
 * The Template for displaying all single posts
  * @see        https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */



namespace App;

use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Post;
use Timber\Timber;

class SingleProductController
{
    public function handle()
    {
        
        $context = Timber::get_context();
        $post = new Post();

        $context['post']    = Timber::get_post();
        $product            = wc_get_product( $context['post']->ID );
        $context['product'] = $product;

        // Terms
        $theme = array();
        $category = array();
        if(get_the_terms( $context['post']->ID, 'ad_product_theme')){
            $theme = get_the_terms( $context['post']->ID, 'ad_product_theme');
        }
        if(get_the_terms( $context['post']->ID, 'product_cat')){
            $category = get_the_terms( $context['post']->ID, 'product_cat');
        }    
        $context['tags'] = array_merge($theme, $category);

        // Get related products
        $related_limit               = wc_get_loop_prop( 'columns' );
        $related_ids                 = wc_get_related_products( $context['post']->id, $related_limit );
        $context['related_products'] =  Timber::get_posts( $related_ids );

        $context['video_id'] = get_field('video_id');

        $context['add_to_cart'] = do_shortcode('[add_to_cart show_price="FALSE" style="none" id="'.$context['post']->ID.'"]');
        $context['header_image'] = get_field('header_image');
        $context['format'] = get_field('format');
        $context['people'] = get_field('people');
        $context['time'] = get_field('time');
        $context['age'] = get_field('age');

        $context['name_your_price'] = get_post_meta($context['post']->ID, '_is_nameyourprice' );

        // Restore the context and loop back to the main query loop.
        wp_reset_postdata();

        // Timber::render( 'views/woo/single-product.twig', $context );
        return new TimberResponse('woo/single-product', $context);
    }
}
