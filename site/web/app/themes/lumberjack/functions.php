<?php

use App\Http\Lumberjack;

// Create the Application Container
$app = require_once('bootstrap/app.php');

// Bootstrap Lumberjack from the Container
$lumberjack = $app->make(Lumberjack::class);
$lumberjack->bootstrap();

require_once('app/Http/Controllers/ClientZone.php');

// Import our routes file
require_once('routes.php');

require_once('library/taxonomies/woo-themes.php');
require_once('library/taxonomies/woo-context.php');
require_once('library/options-page.php');
require_once('library/dashboard-widgets.php');
require_once('library/autocomplete-orders.php');


// Set global params in the Timber context
add_filter('timber_context', [$lumberjack, 'addToContext']);

$AD = array();
$AD['version'] = 1.7; // Use this to force flush js & css cache


// Styles and scripts
function ad_base_scripts() {

    global $AD;

    // modernizr
    // wp_enqueue_script(
    //     'modernizr',
    //     get_template_directory_uri() . '/assets/dist/js/vendor/modernizr-2.8.3.min.js',
    //     false,
    //     '2.8.3',
    //     false
    // );

    // wp_enqueue_style('main-stylesheet', get_stylesheet_directory_uri() . '/assets/css/main.min.css');

    wp_deregister_script('jquery');

    // Load CDN jQuery in the footer
    wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', false, '3.3.1', true);



    // wp_enqueue_script('modernizer');
    wp_enqueue_script('jquery');
    
    // swiper
    wp_enqueue_script(
        'swiper',
        get_template_directory_uri() . '/node_modules/swiper/swiper-bundle.min.js',
        false,
        '1',
        true
    );

    // Site Theme
    wp_enqueue_script(
        'js',
        get_template_directory_uri() . '/assets/js/main.js',
        array('jquery'),
        $AD['version'],
        true
    );

    wp_enqueue_style('futura', 'https://use.typekit.net/qtc3lpn.css', array() );
    wp_enqueue_style('main-stylesheet', get_stylesheet_directory_uri() . '/assets/css/main.min.css', array(), $AD['version'] );

    // wp_localize_script( 'js', 'ad_Ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ){
        wp_enqueue_script( 'comment-reply' );
    }

}
add_action( 'wp_enqueue_scripts', 'ad_base_scripts' );

if( function_exists('acf_add_options_page') ) {
    
    // acf_add_options_page(array(
    //     'page_title'    => 'Misc text',
    //     'menu_title'    => 'Misc text',
    //     'menu_slug'     => 'top-bar',
    //     'capability'    => 'edit_posts',
    //     'redirect'      => false
    // ));
    
}

function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

add_action('template_redirect', 'remove_shop_breadcrumbs' );
function remove_shop_breadcrumbs(){
 
    if (is_shop())
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
 
}

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 15 );
// add_action( 'woocommerce_single_product_summary', array( WC_Name_Your_Price()->display, 'display_price_input' ), 14 );


remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

// Remove each style one by one
add_filter( 'woocommerce_enqueue_styles', 'ad_dequeue_styles' );
function ad_dequeue_styles( $enqueue_styles ) {
    unset( $enqueue_styles['woocommerce-general'] );    // Remove the gloss
    // unset( $enqueue_styles['woocommerce-layout'] );     // Remove the layout
    // unset( $enqueue_styles['woocommerce-smallscreen'] );    // Remove the smallscreen optimisation
    return $enqueue_styles;
}

add_filter ( 'wc_add_to_cart_message', 'wc_add_to_cart_message_filter', 10, 2 );
function wc_add_to_cart_message_filter($message, $product_id = null) {
    $titles[] = get_the_title( $product_id );

    $titles = array_filter( $titles );
    $added_text = sprintf( _n( '%s has been added to your cart.', '%s have been added to your cart.', sizeof( $titles ), 'woocommerce' ), wc_format_list_of_items( $titles ) );

    $message = sprintf( '%s</a>&nbsp;<a href="%s" class="button-secondary">%s</a>&nbsp;<a href="%s" class="button-secondary button-padding-right">%s</a>',
                    esc_html( $added_text ),
                    esc_url( wc_get_page_permalink( 'cart' ) ),
                    esc_html__( 'View Cart', 'woocommerce' ),
                    esc_url( '/resource-hub/' ),
                    esc_html__( 'Continue Browsing', 'woocommerce' ));

    return $message;
}

add_filter ( 'woocommerce_account_menu_items', 'misha_one_more_link' );
function misha_one_more_link( $menu_links ){
 
    // we will hook "anyuniquetext123" later
    $new = array( 'clientzone' => 'Client Zone' );
 
    // or in case you need 2 links
    // $new = array( 'link1' => 'Link 1', 'link2' => 'Link 2' );
 
    // array_slice() is good when you want to add an element between the other ones
    $menu_links = array_slice( $menu_links, 0, 1, true ) 
    + $new 
    + array_slice( $menu_links, 1, NULL, true );
 
 
    return $menu_links;
 
 
}
 
add_filter( 'woocommerce_get_endpoint_url', 'misha_hook_endpoint', 10, 4 );
function misha_hook_endpoint( $url, $endpoint, $value, $permalink ){
 
    if( $endpoint === 'clientzone' ) {
 
        // ok, here is the place for your custom URL, it could be external
        $url = '/client-zone';
 
    }
    return $url;
 
}

function cleanup_attachment_link( $link ) {
    return;
}
add_filter( 'attachment_link', 'cleanup_attachment_link' );

add_action( 'woocommerce_account_downloads_columns', 'custom_downloads_columns', 10, 1 ); // Orders and account
add_action( 'woocommerce_email_downloads_columns', 'custom_downloads_columns', 10, 1 ); // Email notifications
function custom_downloads_columns( $columns ){
    // Removing "Download expires" column
    if(isset($columns['download-expires']))
        unset($columns['download-expires']);

    // Removing "Download remaining" column
    if(isset($columns['download-remaining']))
        unset($columns['download-remaining']);

    return $columns;
}

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

function timber_set_product( $post ) {
    global $product;

    // is_woocommerce() does not return true on the front_page by default!

    if ( is_woocommerce() or is_front_page() ) {
        $product = wc_get_product( $post->ID );
    }
}

add_filter( 'manage_edit-product_cat_columns', 'myRemoveProductCategoryColumns', 99 );
function myRemoveProductCategoryColumns( $columns)
{    
    unset( $columns['thumb'] );
    return $columns;
}

add_filter( 'the_content', 'wpautop' );

function change_related_product_text( $translated ) {
   $translated = str_replace( 'Related products', 'Related resources', $translated );
   return $translated;
}
add_filter( 'gettext', 'change_related_product_text' );


function filter_related_products($args){    
    global $product;
    $args = array();    
    $theme = wc_get_product_terms( $product->id, 'ad_product_theme', array( 'fields' => 'slugs' ) );

    if(!empty($theme)){
        $args = get_posts( array(
            'post_type' => 'product',
            'numberposts' => 3,
            'post_status' => 'publish',
            'post__not_in' => array($product->id),
            'tax_query' => array(
                    array(
                        'taxonomy' => 'ad_product_theme',
                        'field'    => 'slug',
                        'terms'    => $theme[0],
                    ),
            )
       ) );
    }
    
    return $args;
}
add_filter('woocommerce_related_products','filter_related_products');

add_filter( 'gform_ajax_spinner_url', 'spinner_url', 10, 2 );
function spinner_url( $image_src, $form ) {
    return "/app/themes/lumberjack/assets/img/spinner.svg";
}

