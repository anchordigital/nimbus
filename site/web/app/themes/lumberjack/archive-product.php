<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Post;
use App\PostTypes\Story;
use Timber\Timber;

class ArchiveProductController extends Controller
{
    public function handle()
    {
        // die;
        $context            = Timber::get_context();
        $context['sidebar'] = Timber::get_widgets( 'shop-sidebar' );

        $posts = Timber::get_posts();
        $context['products'] = $posts;

        if ( is_product_category() ) {
            $queried_object = get_queried_object();
            $term_id = $queried_object->term_id;
            $context['category'] = get_term( $term_id, 'product_cat' );
            $context['title'] = single_term_title( '', false );
        }

        $context['banner_images'] = get_field('banner_images', '6');

        $context['product_themes'] = get_terms( 'ad_product_theme', array(
            'hide_empty' => true,
        ) );

        $context['product_cats'] = get_terms( 'product_cat', array(
            'hide_empty' => true,
        ) );

        $context['product_context'] = get_terms( 'ad_product_context', array(
            'hide_empty' => true,
        ) );

        $context['page_heading'] = "All Resources";

        // Timber::render( 'views/woo/archive.twig', $context );

        return new TimberResponse('woo/archive', $context);

    }
}
