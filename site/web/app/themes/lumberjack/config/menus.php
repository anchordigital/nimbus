<?php

return [
    /**
     * List of menus to register with WordPress during bootstrap
     */
    'menus' => [
        'main-nav' => __('Main Navigation Logged In'),
        'main-nav-logged-out' => __('Main Navigation Logged Out'),
        'footer-link' => __('Footer Links'),
        'resource-hub' => __('Resource Hub'),
    ],
];
