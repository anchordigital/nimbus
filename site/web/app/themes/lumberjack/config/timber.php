<?php

return [
    /**
     * List of directories to load Twig files from
     */
    'paths' => [
        'resources/patterns',
        'resources/templates',
        'views',
    ],
];
