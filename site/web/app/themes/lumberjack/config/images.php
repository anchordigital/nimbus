<?php

return [
    /**
     * List of image sizes to register, each image size looks like:
     *     [
     *         'name' => 'thumb'
     *         'width' => 100,
     *         'height' => 200,
     *         'crop' => true,
     *     ]
     */
    'sizes' => [
        [
            'name' => 'resource_thumb',
            'width' => 674,
            'height' => 337,
            'crop' => false,
        ]
    ],
];
