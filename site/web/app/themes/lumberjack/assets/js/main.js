function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

(function($) {

    $( "body" ).on( "click", ".form-submit", function(event) {
        // alert('test');  

        event.preventDefault();

        var form = $(this).parent().parent();

        $(form).find('.field-error').remove();
        $(form).find('.form-error').remove();

        var email = $(form).find("input[type='email']");
        var button = $(this).parent();
        var loader = $(form).find(".loader");
        button.attr("disabled", "disabled");
        button.hide();
        loader.show();
        // console.log(email);
        // Validation
        if (!validateEmail(email.val())) {
            // alert('invalid');
            $(email).parent().append("<div class='field-error'>Please enter a valid email address</div>");
            //button.val('Sign Up');
            button.removeAttr("disabled");
            button.show();
            loader.hide();
            return;
        }

        var form_id = form.data('id');
        var fields_array = form.serializeArray();
        var fields_string = form.serialize();

        // $( fields_array ).each(function( index ) {
        //   console.log( $( this ) );
        // });

        // var nonce = $(this).attr("data-nonce");

        $.ajax(
          {
                url : ad_Ajax.ajaxurl,
                data: {
                    action: 'ad_send_email',
                    form_id : form_id,
                    fields : fields_array,
                    // nonce: nonce
                },
                // processData: false,
                type: 'POST',
                success: function(data) {
                    data = JSON.parse(data);
                    console.log(data);
                    if( data.status == 'Ok'){
                        var height = $(".popout-form-container").outerHeight();
                        $(".popout-form-container").css("height", height);
                        $(".popout-form-container").addClass("success");
                        $(".form").addClass("success");
                        $(form).parent().append('<div class="form-success">'+data.message+'</div>');
                        $(form).hide();
                        $(".popout-form-container h2").hide();
                        $(".popout-form-container p").hide();
                    } else {
                        $(form).append('<div class="form-error">'+data.message+'</div>');
                        button.removeAttr("disabled");
                        button.show();
                        loader.hide();
                    }
                },
                error: function(response) {
                    // var button = $('.sign-up');
                    button.val('Sign Up');
                    button.removeAttr("disabled");
                    button.show();
                    loader.hide();
                }
           }
        );

        return false;
    });

})(jQuery);


function reset_project_form(){
    var form = $(".popout-form-container").find('form');
    $(".popout-form-container").removeClass("success");
    $(".form").removeClass("success");
    $(form).find('.form-success').remove();
    $(form)[0].reset();
    $(".field").removeClass('field--active');
    $(form).show();
    $(".popout-form-container h2").show();
    $(".popout-form-container p").show();
    var height = $(".popout-form-container").outerHeight();
    $(".popout-form-container").css("height", "auto");
}

if($('.product-filters__button').length){

	$( ".product-filters__button" ).on( "click", function(e) {
		e.preventDefault();
	  	$(".product-filters__dropdown").toggleClass("open");
	   	$(".product-filters__button").toggleClass("open");
	});

}
if($('.ywcnp_sugg_price').length){
	$(".ywcnp_sugg_price").attr("placeholder", "£ your price");
}
$(document).ready(function(){

    $( "body" ).on( "click", ".nav-icon", function() {
        $(this).toggleClass('open');
        $(".site-header").toggleClass('open');
        $(".site-header__contents").slideToggle();
    });

    if ($(window).width() < 980) {
        var icon = '<div class="nav-icon"> <span></span> <span></span> <span></span> <span></span> <span></span> <span></span></div>';
        $(".site-header__inner").append(icon);

        $( "body" ).on( "click", ".menu-item-has-children", function(e) {

        	e.preventDefault();
        	$(this).parent().toggleClass('reveal');

        });

        if($(".woocommerce-MyAccount-navigation").length){

            $(".woocommerce-MyAccount-navigation").prepend('<a class="account-menu secondary-button" href="#">Account Menu</a>');
            $( "body" ).on( "click", ".account-menu", function() {
                $(".woocommerce-MyAccount-navigation").toggleClass("open");
                $(".woocommerce-MyAccount-navigation ul").slideToggle();
            });

        }
    }

});

jQuery( function( $ ) {
    if ( ! String.prototype.getDecimals ) {
        String.prototype.getDecimals = function() {
            var num = this,
                match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
            if ( ! match ) {
                return 0;
            }
            return Math.max( 0, ( match[1] ? match[1].length : 0 ) - ( match[2] ? +match[2] : 0 ) );
        }
    }
    // Quantity "plus" and "minus" buttons
    $( document.body ).on( 'click', '.plus, .minus', function() {
        var $qty        = $( this ).closest( '.quantity' ).find( '.qty'),
            currentVal  = parseFloat( $qty.val() ),
            max         = parseFloat( $qty.attr( 'max' ) ),
            min         = parseFloat( $qty.attr( 'min' ) ),
            step        = $qty.attr( 'step' );

        // Format values
        if ( ! currentVal || currentVal === '' || currentVal === 'NaN' ) currentVal = 0;
        if ( max === '' || max === 'NaN' ) max = '';
        if ( min === '' || min === 'NaN' ) min = 0;
        if ( step === 'any' || step === '' || step === undefined || parseFloat( step ) === 'NaN' ) step = 1;

        // Change the value
        if ( $( this ).is( '.plus' ) ) {
            if ( max && ( currentVal >= max ) ) {
                $qty.val( max );
            } else {
                $qty.val( ( currentVal + parseFloat( step )).toFixed( step.getDecimals() ) );
            }
        } else {
            if ( min && ( currentVal <= min ) ) {
                $qty.val( min );
            } else if ( currentVal > 0 ) {
                $qty.val( ( currentVal - parseFloat( step )).toFixed( step.getDecimals() ) );
            }
        }

        // Trigger change event
        $qty.trigger( 'change' );
    });
});

jQuery('div.woocommerce').on('change', '.qty', function(){
           jQuery("[name='update_cart']").removeAttr('disabled');
           jQuery("[name='update_cart']").trigger("click"); 
        });
function initImageSliders(){
    
   	if($('.shop-banner').length){

		var imageslider = new Swiper ('.shop-banner', {
	    // Optional parameters
	    direction: 'horizontal',
	    loop: true,
      	spaceBetween: 16,
      	autoplay: {
	        delay: 3500,
	        disableOnInteraction: false,
	      },
      	pagination: {
	        el: '.swiper-pagination',
	        dynamicBullets: true,
	      },
	    // Navigation arrows
	    // navigation: {
	    //   nextEl: '.swiper-button-next',
	    //   prevEl: '.swiper-button-prev',
	    // },

	  })

	}

}
initImageSliders();