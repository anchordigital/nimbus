function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

(function($) {

    $( "body" ).on( "click", ".form-submit", function(event) {
        // alert('test');  

        event.preventDefault();

        var form = $(this).parent().parent();

        $(form).find('.field-error').remove();
        $(form).find('.form-error').remove();

        var email = $(form).find("input[type='email']");
        var button = $(this).parent();
        var loader = $(form).find(".loader");
        button.attr("disabled", "disabled");
        button.hide();
        loader.show();
        // console.log(email);
        // Validation
        if (!validateEmail(email.val())) {
            // alert('invalid');
            $(email).parent().append("<div class='field-error'>Please enter a valid email address</div>");
            //button.val('Sign Up');
            button.removeAttr("disabled");
            button.show();
            loader.hide();
            return;
        }

        var form_id = form.data('id');
        var fields_array = form.serializeArray();
        var fields_string = form.serialize();

        // $( fields_array ).each(function( index ) {
        //   console.log( $( this ) );
        // });

        // var nonce = $(this).attr("data-nonce");

        $.ajax(
          {
                url : ad_Ajax.ajaxurl,
                data: {
                    action: 'ad_send_email',
                    form_id : form_id,
                    fields : fields_array,
                    // nonce: nonce
                },
                // processData: false,
                type: 'POST',
                success: function(data) {
                    data = JSON.parse(data);
                    console.log(data);
                    if( data.status == 'Ok'){
                        var height = $(".popout-form-container").outerHeight();
                        $(".popout-form-container").css("height", height);
                        $(".popout-form-container").addClass("success");
                        $(".form").addClass("success");
                        $(form).parent().append('<div class="form-success">'+data.message+'</div>');
                        $(form).hide();
                        $(".popout-form-container h2").hide();
                        $(".popout-form-container p").hide();
                    } else {
                        $(form).append('<div class="form-error">'+data.message+'</div>');
                        button.removeAttr("disabled");
                        button.show();
                        loader.hide();
                    }
                },
                error: function(response) {
                    // var button = $('.sign-up');
                    button.val('Sign Up');
                    button.removeAttr("disabled");
                    button.show();
                    loader.hide();
                }
           }
        );

        return false;
    });

})(jQuery);


function reset_project_form(){
    var form = $(".popout-form-container").find('form');
    $(".popout-form-container").removeClass("success");
    $(".form").removeClass("success");
    $(form).find('.form-success').remove();
    $(form)[0].reset();
    $(".field").removeClass('field--active');
    $(form).show();
    $(".popout-form-container h2").show();
    $(".popout-form-container p").show();
    var height = $(".popout-form-container").outerHeight();
    $(".popout-form-container").css("height", "auto");
}
