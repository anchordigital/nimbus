function initImageSliders(){
    
   	if($('.shop-banner').length){

		var imageslider = new Swiper ('.shop-banner', {
	    // Optional parameters
	    direction: 'horizontal',
	    loop: true,
      	spaceBetween: 16,
      	autoplay: {
	        delay: 3500,
	        disableOnInteraction: false,
	      },
      	pagination: {
	        el: '.swiper-pagination',
	        dynamicBullets: true,
	      },
	    // Navigation arrows
	    // navigation: {
	    //   nextEl: '.swiper-button-next',
	    //   prevEl: '.swiper-button-prev',
	    // },

	  })

	}

}
initImageSliders();