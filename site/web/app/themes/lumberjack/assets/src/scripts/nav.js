$(document).ready(function(){

    $( "body" ).on( "click", ".nav-icon", function() {
        $(this).toggleClass('open');
        $(".site-header").toggleClass('open');
        $(".site-header__contents").slideToggle();
    });

    if ($(window).width() < 980) {
        var icon = '<div class="nav-icon"> <span></span> <span></span> <span></span> <span></span> <span></span> <span></span></div>';
        $(".site-header__inner").append(icon);

        $( "body" ).on( "click", ".menu-item-has-children", function(e) {

        	e.preventDefault();
        	$(this).parent().toggleClass('reveal');

        });

        if($(".woocommerce-MyAccount-navigation").length){

            $(".woocommerce-MyAccount-navigation").prepend('<a class="account-menu secondary-button" href="#">Account Menu</a>');
            $( "body" ).on( "click", ".account-menu", function() {
                $(".woocommerce-MyAccount-navigation").toggleClass("open");
                $(".woocommerce-MyAccount-navigation ul").slideToggle();
            });

        }
    }

});
