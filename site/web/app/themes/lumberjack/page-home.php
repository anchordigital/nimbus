<?php

/**
* Template name: Home
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Page;
use Timber\Timber;

class PageHomeController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $page = new Page();

        $context['post'] = $page;
        $context['title'] = $page->title;
        $context['content'] = $page->content;

		require_once('template-builders/banner.php');
		$context['themes'] = get_field('themes');
		require_once('template-builders/sections.php');

		$context['intro'] = array(
			array(
				'background' => 'white',
				'blocks' => array(
					array(
						'size' => 'full',
						'background' => get_field('intro_background'),
						'content' => array(
							array(
								'type' => 'text',
								'text' => get_field('intro')
							)
						)
					)
				)
			)
		);


        return new TimberResponse('home', $context);
    }
}
