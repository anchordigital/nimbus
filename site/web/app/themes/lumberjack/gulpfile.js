var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    del = require('del'),
    svgSprite = require('gulp-svg-sprite');
    gutil = require('gulp-util');
    size = require('gulp-size');
    paths = require('path');

// gulp.task('styles', function() {
//   return sass('assets/src/styles/main.scss', { style: 'expanded',
//   loadPath: ['node_modules/breakpoint-sass/stylesheets/']})
//     .pipe(autoprefixer('last 2 version'))
//     // .pipe(gulp.dest('dist/css'))
//     .pipe(gulp.dest('assets/css'))
//     .pipe(rename({suffix: '.min'}))
//     .pipe(cssnano())
//     // .pipe(gulp.dest('dist/css'))
//     .pipe(gulp.dest('assets/css'))
//     .pipe(notify({ message: 'Styles task complete' }));
// });

gulp.task('styles', function() {
  return gulp.src("assets/src/styles/main.scss", { style: 'expanded',
  loadPath: ['node_modules/breakpoint-sass/stylesheets/']})
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest("assets/css"))
    .pipe(rename({suffix: '.min'}))
    .pipe(cssnano({zindex: false}))
    .pipe(gulp.dest('assets/css'))
});


gulp.task('scripts', function() {
  return gulp.src('assets/src/scripts/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    // .pipe(gulp.dest('dist/js'))
    .pipe(gulp.dest('assets/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    // .pipe(gulp.dest('dist/js'))
    .pipe(gulp.dest('assets/js'))
    .pipe(notify({ message: 'Scripts task complete' }));
});

// gulp.task('images', function() {
//   return gulp.src('src/img/**/*')
//     .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
//     .pipe(gulp.dest('dist/img'))
//     .pipe(gulp.dest('../../themes/counterpoint-ad/assets/img'))
//     .pipe(notify({ message: 'Images task complete' }));
// });



// gulp.task('svg', function () {
//     return gulp.src(paths.sprite.src)
//         .pipe($.svgSprite({
//             shape: {
//                 spacing: {
//                     padding: 5
//                 }
//             },
//             mode: {
//                 css: {
//                     dest: "./",
//                     layout: "diagonal",
//                     sprite: paths.sprite.svg,
//                     bust: false,
//                     render: {
//                         scss: {
//                             dest: "assets/src/styles/global/_sprite.scss",
//                             template: "assets/src/build/sprite-template.scss"
//                         }
//                     }
//                 }
//             },
//             variables: {
//                 mapname: "icons"
//             }
//         }))
//         .pipe(gulp.dest(basePaths.dest));
// });

gulp.task('clean', function() {
    return del(['assets/css', 'assets/js']);
});

gulp.task('default', ['clean'], function() {
    gulp.start('styles', 'scripts');
});

gulp.task('watch', function() {

  gulp.start('styles', 'scripts');

  // Watch .scss files
  gulp.watch(['assets/src/styles/**/*.scss','resources/patterns/elements/**/**/*.scss','resources/patterns/elements/**/*.scss','resources/patterns/components/**/*.scss','resources/patterns/components/**/**/*.scss'], ['styles']);

  // Watch .js files
  gulp.watch('assets/src/scripts/**/*.js', ['scripts']);

  // // Watch image files
  // gulp.watch('src/images/**/*', ['images']);

  // // Watch image files
  // gulp.watch('src/img/svg/*', ['svg']);

  // Create LiveReload server
  livereload.listen();

  // Watch any files in dist/, reload on change
  // gulp.watch(['dist/**']).on('change', livereload.changed);


});
