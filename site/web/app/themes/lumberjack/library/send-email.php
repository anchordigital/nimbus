<?php

add_action( 'wp_ajax_ad_send_email', 'ad_send_email' );    // If called from admin panel
add_action( 'wp_ajax_nopriv_ad_send_email', 'ad_send_email' );    // If called from front end

use GuzzleHttp\Exception\ConnectException;

function ad_send_email() {

	$form_id = $_REQUEST['form_id'];
    $fields = $_REQUEST['fields'];
    $subject = get_field( "subject_line", $form_id );
    $send_to = get_field( "send_to", $form_id );
    $success_message = get_field( "success_message", $form_id );
    $add_to_mailing_list = get_field( "add_to_mailing_list", $form_id );
    $opt_in_displayed = get_field( "display_opt_in", $form_id );
    $newsletter_tag = get_field( "mailing_list_tag_id", $form_id );
    // $merge_fields =  array();
    $email = '';
    $name = '';
    // $role = '';
    // $email = '';
    // $country = '';
    $mailing = false;
	// // $admin_email = get_field('meet_us_email', 'option');

	$message = "New form submission:";

    foreach ($fields as $field) {
        $message .= $field['name'] . " : " . $field['value'] . "<br />";

        if( $field['name'] == 'name'){
            $name = $field['value'];
        } elseif( $field['name'] == 'email'  ){
            $email = $field['value'];
        } elseif( $field['name'] == 'opt-in'  ){
            $opt_in = $field['value'];
        } elseif( $field['name'] == 'phone'  ){
            $phone = $field['value'];
        }

    }

    $message =. "<br /><br />Please don't click reply to thsi email, it won't go anywhere. Copy the email address into a new message.";
    
    //  // //if opt in box was present on form check if opt in was ticked
    // // if( $opt_in_displayed ){
    // //     if( $opt_in == 1 ){
    // //         // Do nothing, the process can continue
    // //     } else{
    // //         // Stop the email being added to the mailing list
    // //         $add_to_mailing_list = false;
    // //     }
    // // }

    // // Add to Active Campaign
    // // if($add_to_mailing_list){
    // //     $mailing = true;

    $ac = new ActiveCampaign(getenv('ACTIVE_CAMPAIGN_API_URL'), getenv('ACTIVE_CAMPAIGN_API_KEY'));


    $contact = array(
        "email"              => $email,
        "first_name"         => $first_name,
        "last_name"          => $last_name,
        "phone"          => $phone,
        "p[3]"      => 1,
        "status[3]" => 1, // "Active" status
    );

    $contact_sync = $ac->api("contact/sync", $contact);

    if (!$contact_sync->success) {
        // request failed
        // echo "<p>Syncing contact failed. Error returned: " . $contact_sync->error . "</p>";
        // exit();
        $response['status'] = 'Fail';
    } else {
        // successful request
        $contact_id = $contact_sync->subscriber_id;
        $response['status'] = 'Ok';
        $response['message'] = $success_message;

        // Add tags
        if($ac_tags){
            foreach ($ac_tags as $tag) {
                add_ac_tag($contact_id, $tag['tag']);
            }
        }

        // Check if we should add mailing tags
        if($add_to_mailing_list){
            if( $opt_in_displayed ){
                if( $opt_in == 1 ){ 
                    // User has not opted in, so do not add tag
                    $mailing_list = true;
                }
            } else{
                $mailing_list = true;
            }
        }
        
        if($mailing_list == true){
            add_ac_tag($contact_id, 1);
            add_ac_tag($contact_id, 2);
        }
    }

    $email = wp_mail( $send_to, $subject, $message );

    if($mailing == false){
        $response['status'] = 'Ok';
        $response['message'] = $success_message;
    }

    echo json_encode($response);

    exit();
}

function wpse27856_set_content_type(){
    return "text/html";
}
add_filter( 'wp_mail_content_type','wpse27856_set_content_type' );

?>