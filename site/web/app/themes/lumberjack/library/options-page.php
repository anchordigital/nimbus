<?php
add_action('acf/init', 'ad_acf_op_init');
function ad_acf_op_init() {

    // Check function exists.
    if( function_exists('acf_add_options_page') ) {

        $option_page = acf_add_options_page(array(
            'page_title'    => __('Contact Details'),
            'menu_title'    => __('Contact Details'),
            'menu_slug'     => 'contact-details',
            'capability'    => 'edit_posts',
            'redirect'      => false
        ));

        acf_add_options_page(array(
            'page_title'    => 'Misc text',
            'menu_title'    => 'Misc text',
            'menu_slug'     => 'top-bar',
            'capability'    => 'edit_posts',
            'redirect'      => false
        ));
    }
}