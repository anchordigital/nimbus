<?php

// Responsive videos in admin area
add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    .video-container {
        position: relative;
        padding-bottom: 56.25%;
        padding-top: 30px; height: 0; overflow: hidden;
        }

        .video-container iframe,
        .video-container object,
        .video-container embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        }
        .image-size{
            margin-bottom: 0.5em;
            font-size: 1.1em;
            display: block;
        }
  </style>';
}

add_action('wp_dashboard_setup', 'ad_dashboard_videos');

function ad_dashboard_videos() {
    global $wp_meta_boxes;

    wp_add_dashboard_widget('ad_image_sizes', 'Image sizes', 'ad_image_sizes');
    wp_add_dashboard_widget('ad_themes_video', 'Add new themes/context/category', 'ad_themes_video');
    wp_add_dashboard_widget('ad_product_video', 'Add a product', 'ad_product_video');
    wp_add_dashboard_widget('ad_coupon_video', 'Create a coupon', 'ad_coupon_video');
    wp_add_dashboard_widget('ad_blog_video', 'Add a blog', 'ad_blog_video');
    wp_add_dashboard_widget('ad_client_zone_video', 'Add a Client Zone', 'ad_client_zone_video');
    // wp_add_dashboard_widget('ad_navigation_video', 'Changing the navigation menu', 'ad_navigation_video');
    // wp_add_dashboard_widget('ad_contact_video', 'The contact page', 'ad_contact_video');
    // wp_add_dashboard_widget('ad_story_video', 'Adding a story', 'ad_story_video');
    // wp_add_dashboard_widget('ad_meet_us_video', "Editing the We'd love to meet you section", 'ad_meet_us_video');
    // wp_add_dashboard_widget('ad_takeaway_video', "Add a takeaway", 'ad_takeaway_video');
    // wp_add_dashboard_widget('ad_giving_ask', 'Edit the giving ask', 'ad_giving_ask_video');

}

function ad_image_sizes() {
    echo '<div class="image-sizes">
        <div class="image-size"><strong>Column image: </strong>1200px x 1200px</div>
        <div class="image-size"><strong>Resource and Blog banner: </strong>2400 x 600px</div>
        <div class="image-size"><strong>Resource header and product image: </strong>2400px x 1200px</div>
        <div class="image-size"><strong>Resource and Blog banner: </strong>1200px x 1200px</div>
        <div class="image-size"><strong>Home page images: </strong>These can be changed if desired, but these are the sizes at launch</div>
        <div class="image-size"><strong>Home large: </strong>2400px x 1028px</div>
        <div class="image-size"><strong>Home medium: </strong>2400px x 1676px</div>
        <div class="image-size"><strong>Home small: </strong>2400px x 1676px</div>
        <p>Save images as JPGs, they are smaller file sizes than PNGs. Only use a PNG if the image has transparency in it.</p>
    </div>';
}

function ad_themes_video() {
echo '<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/nseNxyRBFSo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
}

function ad_product_video() {
echo '<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/Zaj5EgUxl_Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
}

function ad_coupon_video() {
echo '<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/oB8g0udDmaY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
}

function ad_blog_video() {
echo '<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/fKx3rHgNqg8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
}

function ad_client_zone_video() {
echo '<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/sfQ8mfkzrlw
" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
}

?>
