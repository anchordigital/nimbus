<?php
// Register Custom Taxonomy
function ad_product_context() {

    $labels = array(
        'name'                       => _x( 'Context', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Context', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Context', 'text_domain' ),
        'all_items'                  => __( 'All contexts', 'text_domain' ),
        'parent_item'                => __( 'Parent context', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent context:', 'text_domain' ),
        'new_item_name'              => __( 'New context Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New context', 'text_domain' ),
        'edit_item'                  => __( 'Edit context', 'text_domain' ),
        'update_item'                => __( 'Update context', 'text_domain' ),
        'view_item'                  => __( 'View context', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate contexts with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove contexts', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular contexts', 'text_domain' ),
        'search_items'               => __( 'Search contexts', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No items', 'text_domain' ),
        'items_list'                 => __( 'contexts list', 'text_domain' ),
        'items_list_navigation'      => __( 'contexts list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'show_in_rest'               => true,
        'rewrite'      => array('slug' => 'resource-hub/context', 'with_front' => false)
    );
    register_taxonomy( 'ad_product_context', array( 'product' ), $args );

}
add_action( 'init', 'ad_product_context', 0 );
?>
