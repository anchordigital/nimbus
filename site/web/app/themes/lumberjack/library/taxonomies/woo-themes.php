<?php
// Register Custom Taxonomy
function ad_product_theme() {

    $labels = array(
        'name'                       => _x( 'Theme', 'Taxonomy General Name', 'text_domain' ),
        'singular_name'              => _x( 'Theme', 'Taxonomy Singular Name', 'text_domain' ),
        'menu_name'                  => __( 'Theme', 'text_domain' ),
        'all_items'                  => __( 'All Themes', 'text_domain' ),
        'parent_item'                => __( 'Parent Theme', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Theme:', 'text_domain' ),
        'new_item_name'              => __( 'New Theme Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Theme', 'text_domain' ),
        'edit_item'                  => __( 'Edit Theme', 'text_domain' ),
        'update_item'                => __( 'Update Theme', 'text_domain' ),
        'view_item'                  => __( 'View Theme', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate Themes with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove Themes', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Themes', 'text_domain' ),
        'search_items'               => __( 'Search Themes', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No items', 'text_domain' ),
        'items_list'                 => __( 'Themes list', 'text_domain' ),
        'items_list_navigation'      => __( 'Themes list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'show_in_rest'               => true,
        'rewrite'      => array('slug' => 'resource-hub/theme', 'with_front' => false)
    );
    register_taxonomy( 'ad_product_theme', array( 'product' ), $args );

}
add_action( 'init', 'ad_product_theme', 0 );
?>
