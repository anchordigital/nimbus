<?php

/**
 * The Template for displaying all single posts
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Post;
use Timber\Timber;

class SingleController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $post = new Post();

        $context['post'] = $post;
        $context['title'] = $post->title;
        $context['content'] = $post->content;

        $context['image'] = get_field('header_image');
        $context['intro'] = get_field('intro');

        $word = str_word_count(strip_tags($post->post_content));
        $m = floor($word / 200);
        $s = floor($word % 200 / (200 / 60));
        if($s > 30){
            $m++;
        }
        $context['time_to_read'] = $m;


        return new TimberResponse('single-post', $context);
    }
}
