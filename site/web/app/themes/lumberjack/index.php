<?php

/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Post;
use Rareloop\Lumberjack\Page;
use Timber\Timber;

class IndexController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $page_for_posts = get_option( 'page_for_posts' );
        $page = new Page($page_for_posts);
        $context['title'] = $page->title;
        $context['banner']['heading'] = get_field('banner_heading', $page->id);
		$context['banner']['image'] = get_field('banner_image', $page->id);
		if(get_field('responsive_images', $page->id) == 'yes'){
			$context['banner']['image_small'] = get_field('banner_image_small', $page->id);
			$context['banner']['image_medium'] = get_field('banner_image_medium', $page->id);
			$context['banner']['image_large'] = get_field('banner_image_large', $page->id);

		} else {
			$context['banner']['image_small'] = get_field('banner_image', $page->id);
			$context['banner']['image_medium'] = get_field('banner_image', $page->id);
			$context['banner']['image_large'] = get_field('banner_image', $page->id);
		}
        $context['posts'] = Post::builder()
	    ->orderBy('date', 'desc')
	    ->get();

        return new TimberResponse('posts', $context);
    }
}
