<?php
/*
Template Name: Woo Template
*/

namespace App;

use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Page;
use Timber\Timber;

class PageWooBusinessController
{
    public function handle()
    {

        $context = Timber::get_context();
        $page = new Page();

        $context['post'] = $page;
        $context['title'] = $page->title;
        $context['content'] = $page->content;

        // $context['header']['title'] = get_field('heading', $page->id);
        // $context['header']['sub_title'] = get_field('heading_content', $page->id);
        // $context['header']['link'] = get_field('heading_link', $page->id);
        // $context['header']['image'] = get_field('heading_background_image', $page->id);
        // $context['header']['retina_image'] = get_field('heading_retina_image', $page->id);


        return new TimberResponse('woo/business', $context);
    }
}
