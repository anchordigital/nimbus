<?php

/**
 * Template name: Contact
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Page;
use App\PostTypes\CaseStudy;
use Rareloop\Lumberjack\Post;
use App\PostTypes\Form;
use App\ViewModels\FormViewModel;
use Timber\Timber;

class PageContactController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $page = new Page();
        $context['post'] = $page;
        $context['title'] = $page->title;
        $context['content'] = $page->content;

        $context['banner']['heading'] = get_field('banner_heading');
        $context['banner']['image'] = get_field('banner_image');

        $context['form'] = do_shortcode('[gravityform id="1" title="false" description="true"]');

        // $form = new Form( get_field('contact_form') );
        // $context['form'] = new FormViewModel($form);

        require_once('template-builders/sections.php');

        return new TimberResponse('contact', $context);
    }
}
