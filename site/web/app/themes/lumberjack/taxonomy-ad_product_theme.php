<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Post;
use App\PostTypes\Story;
use Timber\Timber;

class TaxonomyAdProductThemeController extends Controller
{
    public function handle()
    {
        // die;
        $context            = Timber::get_context();
        $context['sidebar'] = Timber::get_widgets( 'shop-sidebar' );

        $posts = Timber::get_posts();
        $context['products'] = $posts;

        $queried_object = get_queried_object();
        $term_id = $queried_object->term_id;
        $context['category'] = get_term( $term_id, 'ad_product_theme' );
        $context['title'] = single_term_title( '', false );
        

        $context['tax'] = true;

        $context['term_description'] = term_description($term_id);
        $image = get_field('thumbnail', $queried_object);
        $context['term_image'] = wp_get_attachment_image($image, 'full');
        require_once('template-builders/browse-bar.php');

        // Timber::render( 'views/woo/archive.twig', $context );

        return new TimberResponse('woo/archive', $context);

    }
}
